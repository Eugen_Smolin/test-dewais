import { Search } from 'components/Search';
import { User } from 'components/User';
import { Footer } from 'components/Footer';
import { Layout } from './styled';

export const AppLayout = () => {
  return (
    <Layout>
      <Search />
      <User />
      <Footer />
    </Layout>
  );
};
