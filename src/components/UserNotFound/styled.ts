import styled from 'styled-components';

export const Text = styled.div`
  text-align: center;
  font-size: 16px;
  font-style: italic;
`;
