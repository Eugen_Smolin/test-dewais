import { RBox } from 'components/RBox';
import { Text } from './styled';

export const UserNotFound = () => {
  return (
    <RBox>
      <Text>User not found</Text>
    </RBox>
  );
};
