import { ChangeEvent } from 'react';

import { FieldInput } from './styled';

interface RInputProps {
  value: string;
  onChange: (value: string) => void;
  placeholder?: string;
}

export const RInput = ({
  value,
  onChange = () => {},
  placeholder,
}: RInputProps) => {
  const onChangeInput = (event: ChangeEvent<HTMLInputElement>) => {
    onChange(event.target.value);
  };

  return (
    <FieldInput
      value={value}
      onChange={onChangeInput}
      placeholder={placeholder}
    />
  );
};
