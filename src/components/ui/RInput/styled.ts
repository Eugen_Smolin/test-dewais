import styled from 'styled-components';

export const FieldInput = styled.input`
  display: block;
  width: 100%;
  max-width: 300px;
  height: 40px;
  padding: 0 16px;
  font-size: 16px;
  border: 2px solid ${({ theme }) => theme.input.border};
  background-color: ${({ theme }) => theme.input.bg};
  border-radius: 4px;
`;
