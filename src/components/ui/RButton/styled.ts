import styled from 'styled-components';

export const Button = styled.button`
  font-size: 18px;
  border: none;
  color: ${({ theme }) => theme.button.color};
  background: ${({ theme }) => theme.button.bg};
  box-shadow: ${({ theme }) => theme.box.shadow};
  border-radius: 4px;
  padding: 8px 20px;
  cursor: pointer;
`;
