import { ButtonHTMLAttributes, ReactNode } from 'react';

import { Button } from './styled';

interface RButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  onClick?: () => void;
  children: ReactNode;
}

export const RButton = ({ onClick, children }: RButtonProps) => {
  return <Button onClick={onClick}>{children}</Button>;
};
