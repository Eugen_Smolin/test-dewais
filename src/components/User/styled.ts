import styled from 'styled-components';

export const Wrap = styled.div`
  display: flex;
  align-items: center;
  column-gap: 24px;

  @media (max-width: 620px) {
    flex-direction: column;
    align-items: flex-start;
    column-gap: 0;
    row-gap: 24px;
  }
`;

export const Row = styled.div`
  display: grid;
  grid-template-columns: 120px 1fr;
  grid-column-gap: 16px;
  margin-bottom: 24px;

  &:last-child {
    margin-bottom: 0;
  }
`;

export const Name = styled.div`
  color: ${({ theme }) => theme.grey};
`;

export const Link = styled.a`
  color: ${({ theme }) => theme.primary};
  word-break: break-word;
`;

export const Avatar = styled.img`
  display: block;
  width: 150px;
  height: 150px;
  border-radius: 8px;
`;
