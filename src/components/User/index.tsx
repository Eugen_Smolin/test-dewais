import { RLoader } from 'components/RLoader';
import { UserNotFound } from 'components/UserNotFound';
import { RBox } from 'components/RBox';
import { useAppSelector } from 'hooks/redux';
import { userSlice } from 'store/user';
import { StatusEnum } from 'types/enum';
import { Wrap, Row, Name, Link, Avatar } from './styled';

export const User = () => {
  const data = useAppSelector(userSlice.selectors.data);
  const fetchingStatus = useAppSelector(userSlice.selectors.fetchingStatus);

  if (fetchingStatus === StatusEnum.PENDING) return <RLoader />;

  if (!data) return <UserNotFound />;

  return (
    <RBox>
      <Wrap>
        <Avatar
          src={data.avatarUrl}
          alt={data.username}
        />
        <div>
          <Row>
            <Name>Name:</Name>
            <div>{data.name}</div>
          </Row>
          <Row>
            <Name>Username:</Name>
            <div>{data.username}</div>
          </Row>
          <Row>
            <Name>Bio:</Name>
            <div>{data.bio}</div>
          </Row>
          <Row>
            <Name>Link to github:</Name>
            <Link
              href={data.linkUrl}
              target="_blank"
              rel="noreferrer"
            >
              {data.linkUrl}
            </Link>
          </Row>
        </div>
      </Wrap>
    </RBox>
  );
};
