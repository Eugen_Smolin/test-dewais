import styled, { keyframes } from 'styled-components';

const pulse = keyframes`
  0% {
    transform: scale(1);
  }
  50% {
    transform: scale(1.2);
  }
  100% {
    transform: scale(1);
  }
`;

export const Loader = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  width: 100%;
`;

export const Text = styled.div`
  font-weight: 700;
  font-size: 20px;
  line-height: 40px;
  text-transform: uppercase;
  color: ${({ theme }) => theme.primary};
  animation: 1s ${pulse} ease-out infinite;
`;
