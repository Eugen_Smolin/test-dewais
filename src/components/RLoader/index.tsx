import { Loader, Text } from './styled';

export const RLoader = () => {
  return (
    <Loader>
      <Text>Loading...</Text>
    </Loader>
  );
};
