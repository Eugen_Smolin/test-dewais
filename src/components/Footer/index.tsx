import { RBox } from 'components/RBox';
import { Wrap, Link } from './styled';

export const Footer = () => {
  return (
    <Wrap>
      <RBox css={{ textAlign: 'center' }}>
        2023 Eugene Smolin -
        <Link
          href="https://www.linkedin.com/in/eugene-smolin"
          target="_blank"
          rel="noreferrer"
        >
          Linkedin
        </Link>
      </RBox>
    </Wrap>
  );
};
