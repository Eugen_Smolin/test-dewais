import styled from 'styled-components';

export const Wrap = styled.footer`
  margin-top: auto;
`;

export const Link = styled.a`
  display: inline-block;
  color: ${({ theme }) => theme.primary};
  margin-left: 8px;
`;
