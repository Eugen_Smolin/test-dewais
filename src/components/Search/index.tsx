import { useState } from 'react';

import { RBox } from 'components/RBox';
import { RInput } from 'components/ui/RInput';
import { RButton } from 'components/ui/RButton';
import { useAppDispatch } from 'hooks/redux';
import { userSlice } from 'store/user';
import { Header, Wrap } from './styled';

export const Search = () => {
  const dispatch = useAppDispatch();
  const [username, setUsername] = useState('');

  const onSearchUser = () => {
    dispatch(userSlice.thunks.getUserThunk(username));
  };

  return (
    <Header>
      <RBox>
        <Wrap>
          <RInput
            value={username}
            onChange={setUsername}
            placeholder="Username"
          />
          <RButton onClick={onSearchUser}>Search</RButton>
        </Wrap>
      </RBox>
    </Header>
  );
};
