import styled from 'styled-components';

export const Header = styled.header`
  padding-top: 20px;
  margin-bottom: 30px;
`;

export const Wrap = styled.div`
  display: flex;
  column-gap: 8px;
`;
