import { CSSProperties, ReactNode } from 'react';

import { Box } from './styled';

interface RBoxProps {
  css?: CSSProperties;
  children: ReactNode;
}

export const RBox = ({ css, children }: RBoxProps) => {
  return <Box css={{ ...css }}>{children}</Box>;
};
