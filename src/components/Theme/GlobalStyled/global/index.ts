import { createGlobalStyle } from 'styled-components';

import { Reset } from '../reset';
import { Base } from '../base';

export const GlobalStyles = createGlobalStyle`
  ${Reset};
  ${Base};
`;
