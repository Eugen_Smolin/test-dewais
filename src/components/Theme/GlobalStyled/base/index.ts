import { css } from 'styled-components';

export const Base = css`
  body {
    font-family: 'Montserrat', sans-serif;
    font-style: normal;
    font-weight: 400;
    font-size: 14px;
    line-height: 17px;
    color: ${({ theme }) => theme.font.primaryColor};
    background-color: ${({ theme }) => theme.body};
  }
`;
