import React, { ReactNode } from 'react';
import { ThemeProvider } from 'styled-components';

const colors = {
  primary: '#4793FF',
  secondary: '#DAE9FF',
  white: '#FFFFFF',
  black: '#000000',
  grey: '#939CB0',
  shadow: '2px 5px 25px -3px rgba(180, 180, 180, 0.25)',
};

const theme = {
  primary: colors.primary,
  secondary: colors.secondary,
  white: colors.white,
  black: colors.black,
  grey: colors.grey,
  font: {
    primaryColor: colors.black,
  },
  box: {
    bg: colors.white,
    shadow: colors.shadow,
  },
  button: {
    color: colors.white,
    bg: colors.primary,
    shadow: colors.shadow,
  },
  input: {
    border: colors.primary,
    bg: colors.white,
    shadow: colors.shadow,
  },
  body: colors.white,
};

interface ThemeProps {
  children: ReactNode;
}

export const Theme = ({ children }: ThemeProps) => {
  return <ThemeProvider theme={theme}>{children}</ThemeProvider>;
};

export type CustomTheme = typeof theme;
