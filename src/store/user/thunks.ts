import { createAsyncThunk } from '@reduxjs/toolkit';
import axios, { AxiosResponse } from 'axios';

import { http } from 'api/http';
import { WEB_API_ROUTES } from 'api/api-routes';
import { ApiUserRejectType, ApiUserType } from 'types/user';
import { handleServerErrors } from 'utils/prepareAxios';

const getUserThunk = createAsyncThunk<
  ApiUserType,
  string,
  { rejectValue: ApiUserRejectType }
>('userReducer/getUserThunk', async (username: string, { rejectWithValue }) => {
  try {
    const { data } = await http.get(
      WEB_API_ROUTES.getUser.replace('{USERNAME}', username)
    );
    return data;
  } catch (err) {
    if (axios.isAxiosError(err)) {
      const { status, data } = err.response as AxiosResponse;

      if (status !== 404) {
        handleServerErrors(err);
      } else {
        return rejectWithValue(data);
      }
    }

    throw err;
  }
});

export const thunks = {
  getUserThunk,
};
