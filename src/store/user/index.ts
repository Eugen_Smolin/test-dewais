import { createSlice } from '@reduxjs/toolkit';

import { StatusEnum } from 'types/enum';
import { LocalUserType } from 'types/user';
import { UserModel } from 'models/user';
import { selectors } from './selectors';
import { thunks } from './thunks';

export type UserSliceType = {
  data: LocalUserType | null;
  fetchingStatus: StatusEnum;
};

const initialState: UserSliceType = {
  data: null,
  fetchingStatus: StatusEnum.IDLE,
};

const slice = createSlice({
  name: 'weather',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(thunks.getUserThunk.pending, (state) => {
        state.fetchingStatus = StatusEnum.PENDING;
      })
      .addCase(thunks.getUserThunk.fulfilled, (state, { payload }) => {
        if (payload) state.data = UserModel(payload);
        state.fetchingStatus = StatusEnum.SUCCESS;
      })
      .addCase(thunks.getUserThunk.rejected, (state, { payload }) => {
        const notFound = payload?.message.toLowerCase() === 'not found';
        if (notFound) state.data = null;
        state.fetchingStatus = StatusEnum.FAIL;
      });
  },
});

export const userSlice = {
  actions: slice.actions,
  selectors,
  thunks,
};

export default slice.reducer;
