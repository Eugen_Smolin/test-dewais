import { RootState } from 'store';

export const selectors = {
  data: (state: RootState) => state.userReducer.data,
  fetchingStatus: (state: RootState) => state.userReducer.fetchingStatus,
};
