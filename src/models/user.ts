import { ApiUserType, LocalUserType } from 'types/user';

export const UserModel = (data: ApiUserType): LocalUserType => ({
  avatarUrl: data.avatar_url,
  username: data.login,
  name: data.name,
  bio: data.bio,
  linkUrl: data.html_url,
});
