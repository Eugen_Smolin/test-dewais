export type LocalUserType = {
  avatarUrl: string;
  username: string;
  name: string;
  bio: string;
  linkUrl: string;
};

export type ApiUserType = {
  avatar_url: string;
  name: string;
  bio: string;
  html_url: string;
  login: string;
};

export type ApiUserRejectType = {
  documentation_url: string;
  message: string;
};
