import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';

import { store } from 'store';
import { App } from 'App';
import { Theme } from 'components/Theme';
import { GlobalStyles } from 'components/Theme/GlobalStyled';

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);

root.render(
  <React.StrictMode>
    <Provider store={store}>
      <Theme>
        <GlobalStyles />
        <App />
      </Theme>
    </Provider>
  </React.StrictMode>
);
