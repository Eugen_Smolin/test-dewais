import { AxiosError, AxiosResponse } from 'axios';

export const handleServerErrors = (error: AxiosError) => {
  const res = error.response as AxiosResponse;
  console.log('%c => Error ', 'background: red; color: #000', res);
  // error handler
};
